# camera_comm

This is a simple communication protocol between HAL-Robotics system and Innotecuk
robot for CAMERA project.

## How to use it

The server part should start first. Please got to line 18, which looks like:
```
# Bind the socket to the board
server_address = ('192.168.1.203', 10000)
```
and fill the IP address of the computer executing this script, also add a port
(please do not select the already known-port numbers to avoid conflicts)

In the client part, first need to define server's IP and port in line 15
```
# Connect the socket to the port where the server is listening
server_address = ('192.168.1.203', 10000)
```
and then exectute the script.

## Messages

Every message is a JSON string that consists of two part,
{"header": {},"payload": {}}

a. Header
This is always the same and includes:
"header": {"timestamp": %d, "action": %d, "messageID":"%s"}
- the timestamp is the time in seconds since the epoch as a floating point number
- the action is an integer
- the messageId is a UUID number

b. Payload
This is different based on the type of action.

## Types of actions

We have 4 different types of actions that are supported by the protocol.
In this implementation, client sent an action and the server replies with an acknowledgement.

1. move_base
  This actions contains a list of points for the base to follow along.
  A simple waypoints list looks like:
  [{"x": 1, "y": 2, "theta": 0, "d":0}, {"x": 3, "y": 4, "theta": 1.54, "d": 0.50}]

  If it is a waypoint, we should set d (the accepted error) as 0.
  If it is the final target, we should set the d as a realistic number for the
  accepted accuracy.

2. move_to_height
  This action set the hight that the manipulator base should move.
  The payload of this message looks like:
  {"height": 0.50}

3. stabilise
  This is a command to the base to activate stabilisation mechanisms or not.
  So, it's a boolean variable that looks like:
  {"stabilise": 0}

4. stop
  This is a simple stop command. When the robot receives this message, it should
  stop immediately. This is an empty message.


and the acknowledgement message that the server sends when it receives a message
from client. In the payload of the acknowledgement message these is the messageID
of the received message and a status string, either "OK" of "Fail". So, the message
looks like:
  {"status": "OK", "messageReceivedId": "0e345b45-3fd2-463f-9a6e-ed088dfb5377"}

Please check source code for more implementation details.

## Contributing

### Members

- Author from Innotecuk:
Angelos Plastropoulos (angelos.plastropoulos@innotecuk.com)

- Reviewer from Innotecuk:
Artur Gmerek (artur.gmerek@innotecuk.com)
