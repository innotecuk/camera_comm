#!/usr/bin/env python

import json
import socket
import time
import uuid

class Camera_Comm_Server:

    # setup the server
    def __init__(self):

        # Create the TCP/IP socket
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

        # Bind the socket to the board
        server_address = ('192.168.1.203', 10000)
        print 'starting up on %s port %s' % server_address
        self.sock.bind(server_address)

        # Listen for incoming connections
        self.sock.listen(1)

        # Wait for a connection
        print 'waiting for a connection'
        self.connection, self.client_address = self.sock.accept()

    # Parse the input sent by the client
    def parseString(self, inputString):
        parsed_json = json.loads(inputString)

        header = parsed_json['header']
        timestamp = header['timestamp']
        action = header['action']
        messageID = header['messageID']


        if action == 1:
            print("\nI received a move_base action from the client")
            payload = parsed_json['payload']
            waypoints = payload['waypoints']

            print "waypoints are ", [(i['x'], i['y'], i['theta'], i['d']) for i in waypoints]

            error = False
        elif action == 2:
            print("\nI received a move_to_height action from the client")
            payload = parsed_json['payload']
            height = payload['height']

            print "height is ", height

            error = False
        elif action == 3:
            print("\nI received a stabilise action from the client")
            payload = parsed_json['payload']
            activated = payload['stabilise']

            print "activate stabilise is ", activated

            error = False
        elif action == 4:
            print("\nI received a stop action from the client")

            print "Stop NOW!"

            error = False
        else:
            print("I cannot understand the action command")
            error = True

        return error, messageID

    # compose the aknowledgement message to the client
    def aknowledgementCommand(self, status, messageReceivedId):
        timestamp = time.time()
        messageID = uuid.uuid4()
        json_string = '{"header": {"timestamp": %d, "action":%d, "messageID":"%s"},\
         "payload": {"status": "%s", "messageReceivedId": "%s"}' % \
         (timestamp, 0, messageID, status, messageReceivedId)

        print "I'm sending back the aknowledgement '%s'" % json_string
        #print 'timestamp: %d' % timestamp
        #print 'messageID: %s' % messageID
        return json_string

    # spin continuously
    def spin(self):

        try:
            print 'connection from', self.client_address

            while True:
                data = self.connection.recv(512)
                #print data
                if data:
                    errorCode, messageReceivedId = self.parseString(data)

                    if errorCode == True:
                        print ("somtehing went wrong here")
                        aknowledgement_json = self.aknowledgementCommand("Fail", messageReceivedId)
                    else:
                        print ("Everything is OK on my side")
                        aknowledgement_json = self.aknowledgementCommand("OK", messageReceivedId)
                    # send feedback to the client
                    self.connection.sendall(aknowledgement_json)

                else:
                    # we have troubles, let's get out of here
                    print 'No data received from the client', self.client_address
                    break

        finally:
            self.closeServer()

    # turn-off the server
    def closeServer(self):
        print 'closing connection...'
        self.connection.close()
        print 'closing socket...'
        self.sock.close()

if __name__ == '__main__':
    try:
        myServer = Camera_Comm_Server()
        myServer.spin()
    except KeyboardInterrupt:
        pass
    finally:
        print("Bye!")
