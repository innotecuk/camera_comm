#!/usr/bin/env python

import json
import socket
import uuid
import time

class Camera_Comm_Client:

    def __init__(self):
        # Create a TCP/IP socket
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        # Connect the socket to the port where the server is listening
        server_address = ('192.168.1.203', 10000)
        print 'connecting to %s port %s' % server_address
        self.sock.connect(server_address)


    # move base along the waypoints
    def moveBaseCommand(self):
        timestamp = time.time()
        messageID = uuid.uuid4()

        waypoints = [{"x": 1, "y": 2, "theta": 0, "d": 0}, {"x": 3, "y": 4, "theta": 1.54, "d": 0.5}]
        waypoints_json = json.dumps(waypoints)

        json_string = '{"header": {"timestamp": %d, "action": %d, "messageID":"%s"},\
         "payload": {"waypoints": %s}}' % (timestamp, 1, messageID, waypoints_json)

        print 'sending command "%s"' % json_string

        return json_string

    # move manipulator base to certain height
    def moveToHeightCommand(self):
        timestamp = time.time()
        messageID = uuid.uuid4()

        height = 1.54

        json_string = '{"header": {"timestamp": %d, "action": %d, "messageID":"%s"},\
         "payload": {"height": %f}}' % (timestamp, 2, messageID, height)

        print 'sending command "%s"' % json_string

        return json_string

    # stabilise base
    def stabiliseCommand(self):
        timestamp = time.time()
        messageID = uuid.uuid4()

        stabilise = 0

        json_string = '{"header": {"timestamp": %d, "action": %d, "messageID":"%s"},\
         "payload": {"stabilise": %d}}' % (timestamp, 3, messageID, stabilise)

        print 'sending command "%s"' % json_string

        return json_string

    # stop the base
    def stopCommand(self):
        timestamp = time.time()
        messageID = uuid.uuid4()

        json_string = '{"header": {"timestamp": %d, "action": %d, "messageID":"%s"},\
         "payload": {}}' % (timestamp, 4, messageID)

        print 'sending command "%s"' % json_string

        return json_string

    def spin(self):

        while True:
            print "\n>> Please command type [1]:move_base_waypoints/"
            print "[2]:moveToHeight/[3]:stabilise/[4]:stop/[9]:terminate"
            command = raw_input(">> ")


            if int(command) == 1:
                json_string = self.moveBaseCommand()
                print "I'm sending a move_base command"
            elif int(command) == 2:
                json_string = self.moveToHeightCommand()
                print "I'm sending a move_to_height command"
            elif int(command) == 3:
                json_string = self.stabiliseCommand()
                print "I'm sending a stabilise command"
            elif int(command) == 4:
                json_string = self.stopCommand()
                print "I'm sending a stop command"
            elif int(command) == 9:
                break

            # Send message
            self.sock.sendall(json_string)

            # Listen to feedback
            message = self.sock.recv(256)
            print 'I received aknowledgement: "%s"' % message

        self.closeClient()

    def closeClient(self):
        print 'closing socket'
        self.sock.close()

if __name__ == '__main__':
    try:
        myClient = Camera_Comm_Client()
        myClient.spin()
    except KeyboardInterrupt:
        pass
    finally:
        print("Bye!")
